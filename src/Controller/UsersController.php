<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class UsersController extends AppController
{
    // Other methods..

    

    public function login()
    {
        if ($this->request->is('post')) {
            /*$password = "admin123";
            $hasher = new DefaultPasswordHasher();
            $hasher->hash($password);pr($hasher->hash($password));die;
            $username = $this->request->getData()['username'];
            $userstbl = TableRegistry::get('Users');
            $users = $userstbl->find()->where(['username' => $username])->first();*/
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }else{
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}