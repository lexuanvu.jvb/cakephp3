<?php
namespace App\Controller;

use App\Controller\AppController;

class ArticlesController extends AppController
{
     
    public function isAuthorized($user)
    {
        if ($this->request->getParam('action') === 'add') {
            return true;
        }

        if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
            $articleId = (int)$this->request->getParam('pass.0');
            if ($this->Articles->isOwnedBy($articleId, $user['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }

    public function index()
    {
        $this->paginate = [
            'limit' => 5,
            'order' => [
                'Articles.id' => 'desc'
            ]
        ];
        $articles = $this->paginate($this->Articles);

        $this->set('articles', $articles);
    }

    public function view($id = null)
    {
        $article = $this->Articles->get($id);

        $this->set('article', $article);
    }

    public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            $article->user_id = $this->Auth->user('id');
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The Article has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Article could not be saved. Please, try again.'));
        }
        $this->set(compact('article'));
    }

    public function edit($id = null)
    {
        $article = $this->Articles->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The Article has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The Article could not be saved. Please, try again.'));
        }
        $this->set('article', $article);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The Article has been deleted.'));
        } else {
            $this->Flash->error(__('The Article could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
